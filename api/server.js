//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser = require('body-parser')
  app.use(bodyparser.json());
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  })

var movimientosV2JSON = require('./movimientosv2.json');

var requestjson = require('request-json');

var urlClientesMLab = "https://api.mlab.com/api/1/databases/hhernandez/collections/Clientes?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlClientesMLab);

var urlMLabRaiz = "https://api.mlab.com/api/1/databases/hhernandez/collections";
 var apiKey = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt"; 
var clienteMLabRaiz;

var bycrpt = require('bcrypt');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  //res.send('Hemos recibido su petición get')
  res.sendFile(path.join(__dirname, 'index.html'))
})

app.post('/', function(req, res) {
  res.send('Hemos recibido su petición post')
})

app.put('/', function(req, res) {
  res.send('Hemos recibido su petición put')
})

app.delete('/', function(req, res) {
  res.send('Hemos recibido su petición delete')
})

app.get('/v1/Clientes/:idcliente', function(req, res) {
  res.send('Aquí tiene al cliente número: ' + req.params.idcliente)
})

app.post('/v1/Clientes/:idcliente', function(req, res) {
  res.send('Aquí modifica al cliente número: ' + req.params.idcliente)
})

app.put('/v1/Clientes/:idcliente', function(req, res) {
  res.send('Aquí agrega al cliente número: ' + req.params.idcliente)
})

app.delete('/v1/Clientes/:idcliente', function(req, res) {
  res.send('Aquí elimina al cliente número: ' + req.params.idcliente)
})

app.get('/v1/movimientos', function(req, res) {
  res.sendfile('movimientosv1.json');
})

app.get('/v2/movimientos', function(req, res) {
  res.json(movimientosV2JSON);
})

app.get('/v2/movimientos/:index', function(req, res) {
  console.log(req.params.index);
  res.json(movimientosV2JSON[req.params.index-1]);
})

app.get('/v2/movimientosquery', function(req, res) {
  console.log(req.query);
  res.send('recibiendo');
})

app.post('/v2/movimientos', function(req, res) {
  var nuevo = req.body
  nuevo.id = movimientosV2JSON.length + 1
  movimientosV2JSON.push(nuevo)
  res.send('movimiento dado de alta')
})

app.put('/v2/movimientos', function(req, res) {
  res.send('Hemos recibido su petición de actualización de movimientos')
})

app.get('/v3/Clientes', function(req, res) {
  clienteMLab.get('', function(err, resM, body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

app.post('/v3/Clientes', function(req, res) {
  clienteMLab.post('', req.body, function(err, resM, body) {
    res.send(body);
  })
})

//Función para loguear al cliente
app.post('/v5/login', function(req, res) { 
 var email = req.body.email ;
 var password = req.body.password ;

 console.log("Body: " + email + "-" + password);

 var query = 'q={"email":"'+ email +'"}';  

 clienteMLabRaiz = requestjson.createClient(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query); 
 console.log(urlMLabRaiz + "/Usuarios?" + apiKey + "&" + query);  

 clienteMLabRaiz.get('', function(err, resM, body) { 
   if (!err) {
     console.log(body); 
     console.log(body[0].password); 

     bycrpt.compare(password, body[0].password)
     .then(function(samePassword) {
       if(!samePassword) {
           res.status(403).send();
       }
       res.send();
     })
     .catch(function(error){
       console.log("Error authenticating user: ");
       console.log(error);
       next();
     });
   } else { 
     console.log(body); 
   }
 })
  })

//Post Usuarios (insert)
app.post('/v5/register', function(req, res) {
  var nombre = req.body.nombre;
  var apellido = req.body.apellido;
  var email = req.body.email;
  var password = req.body.password;
  var urlLocal = urlMLabRaiz + "/Usuarios?" + apiKey;

  var BCRYPT_SALT_ROUNDS = 12;
  var user = {
    "nombre": nombre,
    "apellido": apellido,
    "email": email,
    "password": password
  };

  clienteMLabRaiz = requestjson.createClient(urlLocal);

  bycrpt.hash(password, BCRYPT_SALT_ROUNDS)
    .then(function(hashedPassword) {
      user.password = hashedPassword;
      clienteMLabRaiz.post('', user, function(err, resM, body) {
        if (!err) {
          res.send(body);
        } else { 
          res.status(404).send('No se pudo registrar usuario'); 
        }
      })
    })
    .catch(function(error){
        console.log("Error saving user: ");
        console.log(error);
    });
})

//Obtiene identificador por colección
app.get('/v5/Identificador/:id', function(req, res) {
  var idCol = req.params.id;
  var coleccion = "";
  var seq = 0;
  var identificador = { };

  if (idCol == "C") { coleccion = "Clientes" }
  if (idCol == "U") { coleccion = "Usuarios" }
  if (idCol == "T") { coleccion = "Cuentas" }
  if (idCol == "M") { coleccion = "Movimientos" }

  if (coleccion != "") {
    var urlLocal = urlMLabRaiz + "/" + coleccion + "?" + apiKey + "&c=true";

    console.log(urlLocal);
    clienteMLabRaiz = requestjson.createClient(urlLocal); 

    clienteMLabRaiz.get('', function(err, resM, body) { 
      console.log(body);
      if (!err) { 
        seq = body + 1;
        identificador.id = idCol + seq;
        res.send(identificador);
      } else { 
        res.status(500).send('Error al recuperar ID'); 
      } 
    })
  } else {
    res.status(500).send('Sin referencia'); 
  }
})

/*Metódos colección Clientes*/
//Get Clientes por ID
app.get('/v5/Clientes/:id', function(req, res) {
  var id = req.params.id;
  var urlLocal = urlMLabRaiz + "/Clientes?" + apiKey + '&q={"idcliente":"'+ id + '"}';

  console.log(urlLocal);
  clienteMLabRaiz = requestjson.createClient(urlLocal); 

  clienteMLabRaiz.get('', function(err, resM, body) { 
    console.log(body);
    if (!err) { 
      if (body.length >= 1) {
        res.send(body);
      }
      else { 
        res.status(404).send('Cliente en conflicto.'); 
      } 
    } else { 
      console.log(body); 
    } 
  })
})

//Post Clientes (insert)
app.post('/v5/Clientes', function(req, res) {
  var urlLocal = urlMLabRaiz + "/Clientes?" + apiKey;
  clienteMLabRaiz = requestjson.createClient(urlLocal); 

  clienteMLabRaiz.post('', req.body, function(err, resM, body) {
    if (!err) {
      res.send(body);
    } else { 
      res.status(404).send('No se pudo registrar cliente'); 
    }
  })
})

/*Metódos colección Usuarios*/
//Post Usuarios (insert)
app.post('/v5/Usuarios', function(req, res) {
  var urlLocal = urlMLabRaiz + "/Usuarios?" + apiKey;
  clienteMLabRaiz = requestjson.createClient(urlLocal); 

  clienteMLabRaiz.post('', req.body, function(err, resM, body) {
    if (!err) {
      res.send(body);
    } else { 
      res.status(404).send('No se pudo registrar usuario'); 
    }
  })
})

/*Metódos colección Cuentas*/
//Get Cuentas por ID
app.get('/v5/Cuentas/:id', function(req, res) {
  var id = req.params.id;
  var urlLocal = urlMLabRaiz + "/Cuentas?" + apiKey + '&q={"idcuenta":"'+ id + '"}';

  console.log(urlLocal);
  clienteMLabRaiz = requestjson.createClient(urlLocal); 

  clienteMLabRaiz.get('', function(err, resM, body) { 
    console.log(body);
    if (!err) { 
      res.send(body);
    } else { 
      res.status(404).send('Cuentas en conflicto'); 
    } 
  })
})

//Post Cuentas (insert)
app.post('/v5/Cuentas', function(req, res) {
  var urlLocal = urlMLabRaiz + "/Cuentas?" + apiKey;
  clienteMLabRaiz = requestjson.createClient(urlLocal); 

  clienteMLabRaiz.post('', req.body, function(err, resM, body) {
    if (!err) {
      res.send(body);
    } else { 
      res.status(404).send('No se pudo registrar cuenta'); 
    }
  })
})

/*Metódos colección Movimientos*/
//Get Movimientos por ID
app.get('/v5/Movimientos/:id', function(req, res) {
  var id = req.params.id;
  var urlLocal = urlMLabRaiz + "/Movimientos?" + apiKey + '&q={"idmovimiento":"'+ id + '"}';

  console.log(urlLocal);
  clienteMLabRaiz = requestjson.createClient(urlLocal); 

  clienteMLabRaiz.get('', function(err, resM, body) { 
    console.log(body);
    if (!err) { 
      res.send(body);
    } else { 
      res.status(404).send('Movimientos en conflicto'); 
    } 
  })
})

//Post Movimientos (insert)
app.post('/v5/Movimientos', function(req, res) {
  var urlLocal = urlMLabRaiz + "/Movimientos?" + apiKey;
  clienteMLabRaiz = requestjson.createClient(urlLocal); 

  clienteMLabRaiz.post('', req.body, function(err, resM, body) {
    if (!err) {
      res.send(body);
    } else { 
      res.status(404).send('No se pudo registrar movimiento'); 
    }
  })
})

/*Consultas complejas*/
app.post('/v5/Compras', function(req, res) {
  console.log(req.body);
  var urlLocal = urlMLabRaiz + "/Movimientos?" + apiKey + "&q=" + JSON.stringify(req.body);

  console.log(urlLocal);
  clienteMLabRaiz = requestjson.createClient(urlLocal); 

  clienteMLabRaiz.get('', function(err, resM, body) { 
    console.log(body);
    if (!err) { 
      res.send(body);
    } else { 
      res.status(404).send('Error al obtener movimientos'); 
    } 
  })
})

app.post('/v5/Negocios', function(req, res) {
  var urlLocal = urlMLabRaiz + "/Movimientos?" + apiKey + "&q=" + JSON.stringify(req.body);

  console.log(urlLocal);
  clienteMLabRaiz = requestjson.createClient(urlLocal); 

  clienteMLabRaiz.get('', function(err, resM, body) { 
    console.log(body);
    if (!err) { 
      res.send(body);
    } else { 
      res.status(404).send('Negocio en conflicto'); 
    } 
  })
})

app.get('/v5/ConnectBBVA', function(req, res) {
  var request = require('request');

  var headersOpt = {
    "content-type": "application/json",
    "Authorization": "Basic YXBwLmJhbmNvbWVyLmNlYzpnUDFWYnBnQTh1I0hqbk1JZmcxZGpUMTBLZEdNNnc0WkxDT0g1UGZrZldXbXA0V0QxaTllR1hMNlFQMDBmTlM0"
  };
  request(
    {
      method:'post',
      url:'https://connect.bbvabancomer.com/token?grant_type=client_credentials',
      headers: headersOpt,
      json: true,
    }, function (error, response, body) {
      if (!error) { 
        res.send(body);
      } else { 
        res.status(404).send('Negocio en conflicto'); 
      }
  });
})

app.get('/v5/BBVAAtms', function(req, res) {
  var request = require('request');
  var cp = req.query.cp;
  var aut = req.headers.authorization;

  var qsOpt = { "postcode": cp };
  var headersOpt = {
    "content-type": "application/json",
    "authorization": "jwt " + aut
  };

  request(
    {
      method:'get',
      uri:'https://apis.bbvabancomer.com/locations_sbx/v1/atms?postcode=' + cp,
      qs: qsOpt,
      headers: headersOpt,
      json: true,
    }, function (error, response, body) {
      if (!error) { 
        res.send(body);
      } else { 
        res.status(404).send('Negocio en conflicto'); 
      }
  });
})
